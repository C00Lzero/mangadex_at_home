package mdnet.base.dao

import com.fasterxml.jackson.annotation.JsonProperty

data class ExportData (
    var identifier: String = "",
    @get:JsonProperty("isCached") var isCached: Boolean = false,
    var executionTimeInMs: Long = 0,
    @get:JsonProperty("isDataSaver") var isDataSaver: Boolean = false,
    var bytesSent: Long = 0,
    @get:JsonProperty("isBrowserCached") var isBrowserCached: Boolean = false,
    var requestIP: String = "",
    var date: Long = 0L,
    var systemByteSent: Long = 0,
    var systemBytesHDD: Long = 0
)

/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
 */
/* ktlint-disable no-wildcard-imports */
package mdnet.base.server

import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.Locale
import mdnet.base.Constants
import mdnet.base.dao.ExportData
import mdnet.base.export.ElasticExporter
import mdnet.base.settings.ClientSettings
import org.http4k.core.Filter
import org.http4k.core.HttpHandler
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.slf4j.LoggerFactory

private val HTTP_TIME_FORMATTER = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss O", Locale.ENGLISH)
private val LOGGER = LoggerFactory.getLogger("Application")
private val elasticExporter = ElasticExporter()

fun addCommonHeaders(): Filter {
    return Filter { next: HttpHandler ->
        { request: Request ->
            val response = next(request)
            response.header("Date", HTTP_TIME_FORMATTER.format(ZonedDateTime.now(ZoneOffset.UTC)))
                    .header("Server", "Mangadex@Home Node ${Constants.CLIENT_VERSION} (${Constants.CLIENT_BUILD})")
        }
    }
}

fun catchAllHideDetails(): Filter {
    return Filter { next: HttpHandler ->
        { request: Request ->
            try {
                next(request)
            } catch (e: Exception) {
                if (LOGGER.isWarnEnabled) {
                    LOGGER.warn("Request error detected", e)
                }
                Response(Status.INTERNAL_SERVER_ERROR)
            }
        }
    }
}

fun timeRequest(exportData: ExportData, clientSettings: ClientSettings): Filter {
    return Filter { next: HttpHandler ->
        { request: Request ->
            val start = System.currentTimeMillis()
            val response = next(request)
            val latency = System.currentTimeMillis() - start
            if (LOGGER.isTraceEnabled && response.header("X-Uri") != null) {
                // Dirty hack to get sanitizedUri from ImageServer
                val sanitizedUri = response.header("X-Uri")
                // Log in TRACE
                if (LOGGER.isInfoEnabled) {
                    LOGGER.info("Request for $sanitizedUri completed in ${latency}ms")
                }
                // Delete response header entirely
                response.header("X-Uri", null)
            }
            if (clientSettings.exportSettings != null) {
                exportData.executionTimeInMs = latency
                exportData.bytesSent = response.body.length!!.toLong()
                exportData.date = System.currentTimeMillis()
                exportData.identifier = clientSettings.exportSettings.exportIdentifier
                elasticExporter.send(exportData, clientSettings)
            }
            // Set response header with processing times
            response.header("X-Time-Taken", latency.toString())
        }
    }
}

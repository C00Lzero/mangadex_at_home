package mdnet.base.export

import com.fasterxml.jackson.databind.ObjectMapper
import mdnet.base.dao.ExportData
import mdnet.base.settings.ClientSettings
import org.http4k.client.ApacheAsyncClient
import org.http4k.core.Method
import org.http4k.core.Request
import org.slf4j.LoggerFactory
import java.util.Base64
import kotlin.concurrent.thread

class ElasticExporter {

    private val LOGGER = LoggerFactory.getLogger("Application")

    fun send(exportData: ExportData, clientSettings: ClientSettings) {
        var authToken = if (!clientSettings.exportSettings?.exportBase64Token.isNullOrEmpty()) {
            clientSettings.exportSettings?.exportBase64Token
        } else null

        authToken = if (authToken == null && !clientSettings.exportSettings?.exportUser.isNullOrBlank() &&
                !clientSettings.exportSettings?.exportPassword.isNullOrEmpty()) {
            val token: String = clientSettings.exportSettings!!.exportUser + ":" + clientSettings.exportSettings.exportPassword
            Base64.getEncoder().encodeToString(token.toByteArray())
        } else
            null

        val asyncClient = ApacheAsyncClient()
        val request = Request(Method.POST, clientSettings.exportSettings!!.exportUrl + ":" + clientSettings.exportSettings.exportPort + "/" + clientSettings.exportSettings.exportIndex + "/_doc/")
                .header("Content-Type", "application/json" )
                .let {
                    if (authToken != null) {
                        it.header("Authorization", "Basic " + authToken)
                    } else {
                        it
                    }
                }
                .body( ObjectMapper().writeValueAsString(exportData))
        asyncClient(request) {
            if (LOGGER.isInfoEnabled)
                LOGGER.info("ASYNC Statistics send:" + it.status)
            if (LOGGER.isTraceEnabled)
                LOGGER.info("ASYNC body: " + it.body)
        }
        // ... must be closed
        thread {
            Thread.sleep(500)
            asyncClient.close()
        }
    }
}
